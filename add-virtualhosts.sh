#!/bin/bash
#This script will add 2 virtual hosts to existing CentOS 7 apache server on ports 8888 and 8000

echo "This script will add 2 virtual hosts to existing CentOS 7 apache server"
echo "Opening firewall ports"
sudo firewall-cmd --zone=public --add-port=8888/tcp --permanent
sudo firewall-cmd --reload
sudo firewall-cmd --zone=public --add-port=8000/tcp --permanent
sudo firewall-cmd --reload

echo "Making directories for conf files"
sudo mkdir /etc/httpd/sites-available /etc/httpd/sites-enabled

echo "Adding strings to httpd.conf to listen additional ports"
sudo sh -c "echo 'Listen 8888' >> /etc/httpd/conf/httpd.conf"
sudo sh -c "echo 'Listen 8000' >> /etc/httpd/conf/httpd.conf"

# sudo sed -i '$ IncludeOptional sites-enabled/*.conf' /etc/httpd/conf/httpd.conf 

echo "Making directories for site configs"
sudo mkdir -p /var/www/site8888
sudo mkdir -p /var/www/site8000

echo "Creating index.html files"
sudo touch /var/www/site8888/index.html
sudo touch /var/www/site8000/index.html

echo "Writing index.html files for both sites"
sudo tee -a /var/www/site8888/index.html << END
<html><body><h1>It works!</h1>
<p>This is web server on 8888 port.</p>
</body></html>
END

sudo tee -a /var/www/site8000/index.html << END
<html><body><h1>It works!</h1>
<p>This is web server on 8000 port.</p>
</body></html>
END

echo " Setting permissions for site folders"
sudo chown -R apache:apache /var/www
sudo chmod -R 755 /var/www

echo "Creating configuration files for sites"
sudo touch /etc/httpd/sites-available/site8888.conf
sudo touch /etc/httpd/sites-available/site8000.conf

echo "Making links"
sudo ln -s /etc/httpd/sites-available/site8888.conf /etc/httpd/sites-enabled/site8888.conf
sudo ln -s /etc/httpd/sites-available/site8888.conf /etc/httpd/sites-enabled/site8888.conf

echo "Writing virtualhost scripts"
sudo tee -a /etc/httpd/sites-enabled/site8888.conf << END
<VirtualHost *:8888>
        ServerName site8888
        ServerAlias site8888
        DocumentRoot    /var/www/site8888
</VirtualHost>
END

sudo tee -a /etc/httpd/sites-enabled/site8000.conf << END
<VirtualHost *:8000>
        ServerName site8000
        ServerAlias site8000
        DocumentRoot    /var/www/site8888
</VirtualHost>
END

echo "Restarting apache to take effect"
sudo systemctl restart httpd

echo "Cheking web server status"
sudo systemctl status httpd

echo "Check working web server on http://*address-of-server*:8888 and http://*address-of-server*:8000"