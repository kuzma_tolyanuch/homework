#!/bin/bash
# This script will install Apache, MySQL, PHP (LAMP) stack on Ubuntu 16.04

echo "This script will install Apache, MySQL, PHP (LAMP) stack on Ubuntu 16.04"

# Install Apache
sudo apt-get install -y apache2

# Change Listen port for apache
sudo sed -i 's/80/8080/' /etc/apache2/ports.conf
sudo sed -i 's/80/8080/' /etc/apache2/sites-enabled/000-default.conf

# Firewall Configurations, open ports
sudo ufw app list
sudo ufw app info "Apache Full"
sudo ufw allow in "Apache Full"
sudo ufw allow 8080

# Install MySQL Server
sudo apt-get install -y mariadb-server mariadb-client

# Install PHP
sudo apt-get install -y php libapache2-mod-php php-mcrypt php-mysql

# Permissions for /wwww
sudo chown -R apache:apache /var/www
sudo chmod -R 755 /var/www

# Autoloading services
sudo systemctl enable httpd
sudo systemctl enable mariadb

# Start services
echo "Starting services"
sudo systemctl start mariadb httpd

# Status
sudo systemctl status httpd
sudo systemctl status mariadb

echo "Check working web server on http://*address-of-server*:8080"

