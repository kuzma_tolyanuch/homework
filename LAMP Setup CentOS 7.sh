#!/bin/bash
# This script will install Apache, MySQL, PHP (LAMP) stack on CentOS 7

echo "This script will install Apache, MySQL, PHP (LAMP) stack on CentOS 7"

# Install Apache
echo "Installing apache web server"
sudo yum install -y httpd

# Change Listen port for apache
echo "Changing apache listening port to 8080"
sudo sed -i 's/80/8080/' /etc/httpd/conf/httpd.conf

# Firewall Configurations, open ports
echo "Opening 8080 port in firewall"
sudo yum install -y firewalld
sudo systemctl start firewalld
sudo firewall-cmd --zone=public --add-port=8080/tcp --permanent
sudo firewall-cmd --reload

# Install MySQL Server
echo "Installing MySQL server"
sudo yum install -y mariadb-server

# Install PHP
echo "Insatlling php environment"
sudo yum install -y php 

# Permissions for /wwww
echo "Changing owner and perminssions to apache"
sudo chown -R apache:apache /var/www
sudo chmod -R 755 /var/www

# Autoloading services
sudo systemctl enable httpd
sudo systemctl enable mariadb

# Starting services
echo "Starting services"
sudo systemctl start mariadb httpd

# Status
sudo systemctl status httpd
sudo systemctl status mariadb

echo "Check working web server on http://*address-of-server*:8080"

