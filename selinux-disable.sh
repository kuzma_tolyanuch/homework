# !/bin/bash
echo "This script will disable selinux on CentOS 7"
echo ""
echo "Setting selinux to disabled" 
	sudo sed -i 's/^SELINUX=.*/SELINUX=disabled/g' /etc/selinux/config
echo ""
echo ""
echo "Checking config file"
	cat /etc/selinux/config
echo ""
echo ""
echo "Done"
echo "In order for the changes to take effect, you need to reboot your environment" 


